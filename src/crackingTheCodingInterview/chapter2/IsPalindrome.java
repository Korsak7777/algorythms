package crackingTheCodingInterview.chapter2;

import java.util.Iterator;
import java.util.LinkedList;

public class IsPalindrome {
    static <T> boolean isPalindrome(LinkedList<T> l) {
        int mid = l.size() / 2 + 1;
        Iterator<T> frontIt = l.iterator();
        Iterator<T> lastIt = l.descendingIterator();

        int i = 0;
        while (i <= mid){
            if (!frontIt.next().equals(lastIt.next()))
                return false;
            i++;
        }
        return true;
    }

    public static void main(String arg[]) {
        LinkedList<Integer> l = new LinkedList<>();
        l.add(1);
        l.add(4);
        l.add(4);
        l.add(4);
        l.add(1);

        System.out.println(isPalindrome(l));

        isPalindrome(l);
    }

}
