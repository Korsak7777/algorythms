package crackingTheCodingInterview.chapter2;

import java.util.Iterator;
import java.util.LinkedList;

public class PartSort {
    static <T extends Comparable> LinkedList<T> partSort(LinkedList<T> l, T m){
        LinkedList<T> res = new LinkedList<>();
        Iterator i = l.iterator();
        while (i.hasNext()){
            T e = (T) i.next();
            if(e.compareTo(m) == -1){ //e<m
                res.addFirst(e);
            } else {
                res.addLast(e);
            }
        }
        return res;
    }

    public static void main(String arg[]){
        LinkedList<Integer> l = new LinkedList<>();
        l.add(1);
        l.add(4);
        l.add(2);
        l.add(8);
        l.add(0);

        System.out.println(partSort(l, 3));
    }
}
