package crackingTheCodingInterview.chapter2

import java.util.*

fun LinkedList<Any>.removeDuplicates() {
    val iterator = this.iterator()

    var i = 0
    while (iterator.hasNext()) {
        val elem = iterator.next()
        val innerIterator = this.listIterator(++i)

        while (innerIterator.hasNext())
            if (innerIterator.next().equals(elem))
                innerIterator.remove()
    }
}

fun main(){
    val x = LinkedList<Any>()
    x.addAll(listOf(1,3,3,1,4,5))
    x.removeDuplicates()
    print(x)
}