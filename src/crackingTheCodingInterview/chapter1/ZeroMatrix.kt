package crackingTheCodingInterview.chapter1

fun zeroMatrix(s: Array<IntArray>): Array<IntArray>{
    val row = IntArray(s.size) {1}
    val column = IntArray(s[0].size) {1}
    
    return s.mapIndexed { hIndex, h ->
        h.forEachIndexed { vIndex, it ->
            if (it == 0){
                row[hIndex] = 0
                column[vIndex] = 0
            }
        }
        if(row[hIndex] == 0)
            IntArray(s[0].size){0}
        else
            h
    }.mapIndexed { index, arr ->
        if(row[index] != 0)
            arr.mapIndexed { index, i -> i* column[index] }.toIntArray()
        else
            arr
    }.toTypedArray()
}

fun main(){
    val a = arrayOf(
            intArrayOf(1,2,0,3),
            intArrayOf(1,1,2,2),
            intArrayOf(1,0,2,1)
    )

    zeroMatrix(a).forEach{
        it.forEach(::print)
        println()
    }
}