package crackingTheCodingInterview.chapter1

import java.awt.image.DataBufferByte
import java.awt.image.WritableRaster
import java.io.File
import java.lang.Enum
import java.util.*
import javax.imageio.ImageIO
import kotlin.math.sqrt


val format = "tif"
val dolfinFile = """C:\Users\Dmitriy\IdeaProjects\algorythms\src\crackingTheCodingInterview\chapter1\dolfin.$format"""
val dolfinRotateFile = """C:\Users\Dmitriy\IdeaProjects\algorythms\src\crackingTheCodingInterview\chapter1\dolfinRotate.$format"""

fun pi4Rotation(picture1: ByteArray): ByteArray{
    val N = sqrt(picture1.size.toDouble()).toInt()

    val picture = picture1.copyOf(N*N)

    val rotPicture = Array(N){ ByteArray(N)}

    picture.forEachIndexed { index, byte ->
        val rowIndex = index/(picture.size/N)
        val columnIndex = index%N

        rotPicture[N-1-rowIndex][N-1-columnIndex] = byte
    }

    return rotPicture.reduce { a1, a2 -> a1.plus(a2) }
}


fun main(){

    Enum
    val data = ImageIO
            .read(File(dolfinFile).inputStream())
            .getRaster()
            .dataBuffer as DataBufferByte

    File(dolfinRotateFile).outputStream().write(
            pi4Rotation(
                    data.getData()
            )
    )
}