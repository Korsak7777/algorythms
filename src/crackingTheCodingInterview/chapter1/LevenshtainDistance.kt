package crackingTheCodingInterview.chapter1

fun isLevenshtainDistanceOneChar(s1:String, s2:String):Boolean{
    val lengthDiff = s1.length - s2.length
    var i1 = 0
    var i2 = 0
    var diffCount = 0
    if (lengthDiff == 1 || lengthDiff == -1){
        while (i1 < s1.length && i2 < s2.length){
            if(s1[i1] == s2[i2]){
                i1++
                i2++
            } else {
                diffCount++
                if (diffCount > 1)
                    return false
                if(lengthDiff == 1)
                    i1++
                else
                    i2++
            }
        }
        return true
    } else if(lengthDiff == 0){
        while (i1 < s1.length && i2 < s2.length){
            if(s1[i1] == s2[i2]){
                i1++
                i2++
            } else {
                diffCount++
                if (diffCount > 1)
                    return false
                i1++
                i2++
            }
        }
        return true
    } else return false
}

fun main(){
    println(isLevenshtainDistanceOneChar("qwer", "qwe"))
    println(isLevenshtainDistanceOneChar("qwer", "qwes"))
    println(isLevenshtainDistanceOneChar("qwer", "qwee"))
    println(isLevenshtainDistanceOneChar("qwer", "qse"))
}