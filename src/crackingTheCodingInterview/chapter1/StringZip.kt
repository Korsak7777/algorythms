package crackingTheCodingInterview.chapter1

fun stringZip(s:String):String {
    tailrec fun x(s:String, index:Int, result:String):String{
        val c = s[index]
        var i = index + 1
        if(i == s.length)
            return if (i-index == 1) result+c else result+c+(i-index)
        while (s[i] == c)
            if(i == s.length)
                return if (i-index == 1) result+c else result+c+(i-index)
            else
                i++
        return if (i-index == 1) x(s, i, result+c) else x(s,i, result+c+(i-index))
    }
    return x(s,0, "")
}

fun main(){
    println(stringZip("wwesfdfghasccc    Nnaseert"))
}