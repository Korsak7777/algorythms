package crackingTheCodingInterview.chapter4;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class BinaryTreeToList {
    static <T> List<LinkedList<Node<T>>> binaryTreeToList(Node<T> n){
        List res = new LinkedList();
        res.add(new LinkedList<Node<T>>());

        x(n, res.iterator(), res);
        return res;
    }

    private static <T> void x(Node<T> n, Iterator<List<Node<T>>> i, List<List<Node<T>>> res){
        List<Node<T>> curr = i.next();
        curr.add(n);

        if(n.children.length == 0)
            return;
        else {
            if(!i.hasNext()){
                List<Node<T>> newLevel = new LinkedList<>();
                res.add(newLevel);
            }
            for(Node<T> c :n.children){
                x(c,i,res);
            }
        }
    }

    public static void main(String[] a){
        Node<Integer> n = new Node<>(1);
        n.children = new Node[]{new Node(11),new Node(12)};
        n.children[0].children = new Node[]{new Node(21),new Node(21)};
        n.children[1].children = new Node[]{new Node(22)};

        System.out.println(binaryTreeToList(n));
    }
}
