package crackingTheCodingInterview.chapter4;

import java.util.Arrays;

public class SortArrayToBynaryTree {
    public static BNode<Integer> arrayToBinaryTree(int[] a){
        if (a.length == 1)
            return new BNode<>(a[0]);

        int mid = a.length/2;
        BNode root = new BNode(a[mid]);
        root.leftNode = arrayToBinaryTree(Arrays.copyOfRange(a, 0, mid));
        if (a.length > mid+1)
            root.rightNode = arrayToBinaryTree(Arrays.copyOfRange(a, mid+1, a.length));

        return root;
    }

    public static void main(String[] a){
        System.out.println(arrayToBinaryTree(new int[] {1,2,3,4,5,6,7}));
        System.out.println(arrayToBinaryTree(new int[] {1,2,3,4,5,6}));
    }
}
