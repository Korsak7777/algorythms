package crackingTheCodingInterview.chapter4;

public  class BNode<T> {
    public T val;
    public BNode(T val){
        this.val = val;
    }
    public T leftNode;
    public T rightNode;

    @Override
    public String toString() {
        return "Node{" +
                "val=" + val +
                ", leftNode=" + leftNode +
                ", rightNode=" + rightNode +
                '}';
    }
}
