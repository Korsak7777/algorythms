package crackingTheCodingInterview.chapter4;

import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class IsBalancedTree {
    static int minLevel = Integer.MAX_VALUE;
    static int maxLevel = Integer.MIN_VALUE;
    static <T> boolean isBalanced(Node<T> n){
        isBalancedHelper(n, 0);
        if(maxLevel - minLevel > 1)
            return false;
        else
            return true;
    }

    static private  <T> void isBalancedHelper(Node<T> n, int currentLevel){
        if(n.children == null){
            if(currentLevel < minLevel)
                minLevel = currentLevel;
            if(currentLevel > maxLevel)
                maxLevel = currentLevel;
        } else
            for (Node<T> c: n.children)
                isBalancedHelper(c, currentLevel + 1);
    }

    public static void main(String[] a){
        Node<Integer> n = new Node<>(1);
        n.children = new Node[]{new Node(11),new Node(12)};
        n.children[0].children = new Node[]{new Node(21),new Node(21)};
        n.children[1].children = new Node[]{new Node(22)};
        n.children[1].children[0].children = new Node[]{new Node(32)};
        n.children[1].children[0].children[0].children = new Node[]{new Node(42)};

        System.out.println(isBalanced(n));
    }
}
