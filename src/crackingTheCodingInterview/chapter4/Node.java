package crackingTheCodingInterview.chapter4;

public class Node <T> {
    public T val;
    public Node(T val){
        this.val = val;
    }
    Node<T>[] children;
}
