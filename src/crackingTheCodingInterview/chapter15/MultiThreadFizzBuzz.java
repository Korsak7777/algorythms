package crackingTheCodingInterview.chapter15;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.*;

public class MultiThreadFizzBuzz {

    public static void fizzBuzz(int n){
        int fizz = 3;
        int buzz = 5;

        ArrayBlockingQueue<Integer> q = new ArrayBlockingQueue(1);

        ExecutorService es = Executors.newFixedThreadPool(4);
        es.execute(() -> {
            int i = 1;
            while (i < n) {
                try {
                    q.put(i++);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        es.execute(() -> {
            while (true)
                if (q.element() % fizz == 0 && q.element() % buzz != 0){
                    try {
                        q.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("fizz");
                }

        });

        es.execute(() -> {
            while (true){
                if (q.element() % buzz == 0 && q.element() % fizz != 0){
                    try {
                        q.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("buzz");

                }
            }
        });

        es.execute(() -> {
            while (true)
                if (q.element() % fizz == 0 && q.element() % buzz == 0){
                    try {
                        q.take();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    System.out.println("fizz buzz");
                }
        });
    }

    public static void main(String[] args) throws InterruptedException {
//        fizzBuzz(21);
        ArrayList list = new ArrayList();
        LinkedBlockingQueue queue = new LinkedBlockingQueue(4);
        queue.put(21);
        queue.put(22);
        queue.put(23);

        queue.contains(22);
        queue.contains(20);
        queue.drainTo(list);
        queue.put(24);
        queue.put(25);
        queue.put(26);
        queue.offer(27); //queue is full now
        queue.peek();
        queue.poll();
        queue.take();
        queue.add(26);
        queue.remove(25);
        queue.remove(25);
        queue.toArray(new Object[4]);





        Executors.newCachedThreadPool();
        Executors.newFixedThreadPool(5);
        Executors.newScheduledThreadPool(5);
        Executors.newSingleThreadExecutor();
        Executors.newSingleThreadScheduledExecutor();
        Executors.defaultThreadFactory();
        Executors.privilegedThreadFactory();
    }
}
