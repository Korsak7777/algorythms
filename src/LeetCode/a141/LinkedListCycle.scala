package LeetCode.a141

import helpStructure.scala.ListNode

object LinkedListCycle {
  def hasCycle(head: ListNode): Boolean = {
    if(head == null || head.next == null)
      false
    else{
      var i1 = head
      var i2 = head.next

      while (true){
        if(i1 == i2)
          return true
        i1 = i1.next
        if(i2.next == null || i2.next.next== null)
          return false
        i2 = i2.next.next
      }
      false
    }
  }
}
