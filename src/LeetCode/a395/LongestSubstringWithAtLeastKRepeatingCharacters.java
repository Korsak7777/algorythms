package LeetCode.a395;


import java.util.*;
import java.util.stream.Collectors;

public class LongestSubstringWithAtLeastKRepeatingCharacters {
    public static int longestSubstring(String s, int k){
        int n = s.length();
        if(k>n)
            return 0;
        int longestSubLength = 0;

        HashMap<Character, List<Integer>> map = new HashMap();

        for(int i = 0; i < s.length(); i++){
            char c = s.charAt(i);
            List<Integer> l = map.getOrDefault(c, new ArrayList());
            l.add(i);
            map.put(c,l);
        }

        List<Integer> badCharInd = map.values().stream()
                .filter(it -> it.size() < k)
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
        if(badCharInd.isEmpty())
            return s.length();
        badCharInd.add(n);

        int start = 0;
        for(int end : badCharInd){
            if(end - start <= longestSubLength || end - start < k){
                start = end+1;
                continue;
            } else {
                longestSubLength = Math.max(longestSubLength, longestSubstring(s.substring(start, end), k));
                start = end;
            }
        }
        return longestSubLength;
    }

    private static int x(String s, int k) {
        return isGood(s,k) ? s.length() : Integer.MIN_VALUE;
    }

    private static boolean isGood(String s, int k) {
        HashMap<Character, Integer> map = new HashMap<>();
        for(char c : s.toCharArray()){
            map.put(c, map.getOrDefault(c, 0)+1);
        }
        Optional<Integer> min = map.values().stream().min(Integer::compareTo);
        return min.isPresent() ? min.get()>=k : false;
    }

    public static void main(String[] args) {
        int x = longestSubstring("bbaaacbd", 3);
        System.out.println(x);
    }

}
