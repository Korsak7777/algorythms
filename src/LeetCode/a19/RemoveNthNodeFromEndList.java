package LeetCode.a19;


import helpStructure.ListNode;

//    Given linked list: 1->2->3->4->5, and n = 2.
//                       1->2->3->5
//        After removing the second node from the end
public class RemoveNthNodeFromEndList {

    public static ListNode removeNthFromEnd(ListNode head, int n) {
        ListNode dummy = new ListNode(0);
        dummy.next = head;
        int length  = 0;
        ListNode first = head;
        while (first != null) {
            length++;
            first = first.next;
        }
        length -= n;
        first = dummy;
        while (length > 0) {
            length--;
            first = first.next;
        }
        first.next = first.next.next;
        return dummy.next;
    }

    public static void main(String[] args) {
        ListNode x = new ListNode(1);
        x.next = new ListNode(2);
        x.next.next = new ListNode(3);
        x.next.next.next = new ListNode(4);
        x.next.next.next.next = new ListNode(5);

        System.out.println(removeNthFromEnd(x, 2));
    }
}
