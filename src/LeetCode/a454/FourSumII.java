package LeetCode.a454;

import java.util.HashMap;

public class FourSumII {
    /**
     * Given four lists A, B, C, D of integer values,
     * compute how many tuples (i, j, k, l) there are such
     * that A[i] + B[j] + C[k] + D[l] is zero.
     */
    public static int fourSumCount(int[] A, int[] B, int[] C, int[] D) {
        int n = A.length;
        int res = 0;

        HashMap<Integer, Integer> map = new HashMap<>(A.length * B.length);
        for (int a : A){
            for(int b : B){
                int key = a + b;
                map.put(key, map.getOrDefault(key, 0) + 1);
            }
        }

        for(int c : C){
            for (int d : D){
                int key = c + d;
                res += map.getOrDefault(-key, 0);
            }
        }
        return res;
    }

    public static void main(String[] args) {

    }
}
