package LeetCode.a20;

import java.util.HashMap;
import java.util.Stack;

public class ValidParentheses {
    // Hash table that takes care of the mappings.
    private HashMap<Character, Character> mappingsCloseOpen;
    private HashMap<Character, Character> mappingsOpenClose;

    // Initialize hash map with mappings. This simply makes the code easier to read.
    public ValidParentheses() {
        this.mappingsCloseOpen = new HashMap<Character, Character>();
        this.mappingsCloseOpen.put(')', '(');
        this.mappingsCloseOpen.put('}', '{');
        this.mappingsCloseOpen.put(']', '[');

        this.mappingsOpenClose = new HashMap<Character, Character>();
        this.mappingsOpenClose.put('(', ')');
        this.mappingsOpenClose.put('{', '}');
        this.mappingsOpenClose.put('[', ']');
    }

    public boolean isValid(String s) {
        if(s.isEmpty())
            return true;

        Stack<Character> stack = new Stack();
        for(char bracket : s.toCharArray()){
            if(mappingsOpenClose.containsKey(bracket)){
                stack.push(bracket);
            } else if(mappingsCloseOpen.containsKey(bracket)){ //)}]
                if(stack.empty())
                    return false; 
                char popBracket = stack.pop();
                if (mappingsCloseOpen.get(bracket) != popBracket){
                    return false;
                }
            }
        }
        if(stack.empty())
            return true;
        else
            return false;
    }
}
