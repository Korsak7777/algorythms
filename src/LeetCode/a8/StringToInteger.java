package LeetCode.a8;

//        Input: "42"
//        Output: 42

//        Input: "   -42"
//        Output: -42

//        Input: "4193 with words"
//        Output: 4193

//        Input: "words and 987"
//        Output: 0
//        Explanation: The first non-whitespace character is 'w', which is not a numerical
//        digit or a +/- sign. Therefore no valid conversion could be performed.

//        Input: "-91283472332"
//        Output: -2147483648
public class StringToInteger {
    public static int myAtoi(String str) {
        int defaultReturn = 0;

        if (str.isEmpty())
            return defaultReturn;
        class H {
            //"   -42" => 4
            int findFirstNonWhitespaceSym(String str) {
                for (int i = 0; i < str.length(); i++)
                    if (str.charAt(i) != ' ')
                        return i;
                return -1;
            }
        }
        int firstIndex = new H().findFirstNonWhitespaceSym(str);
        if (firstIndex == -1)
            return defaultReturn;
        String str1 = str.substring(firstIndex);

        class H1 {
            //"42" => true; "-42" => true;
            //"4193 with words" => true;
            //"words and 987" => false
            boolean isDigit(String str) {
                char f = str.charAt(0);

                if (str.length() == 1)
                    return Character.isDigit(f);

                char s = str.charAt(1);
                if (Character.isDigit(f) ||
                        (f == '+' && Character.isDigit(s)) ||
                        (f == '-' && Character.isDigit(s)))
                    return true;
                else
                    return false;
            }
        }
        if (new H1().isDigit(str1)) {
            class H3 {
                int findLastNumericalSym(String str) {
                    int j;
                    class H5 {
                        boolean haveSign(String str) {
                            char f = str.charAt(0);
                            if (Character.isDigit(f))
                                return false;
                            else
                                return true;
                        }
                    }
                    if (new H5().haveSign(str))
                        j = 1;
                    else
                        j = 0;

                    for (int i = j; i < str.length(); i++)
                        if (!Character.isDigit(str.charAt(i)))
                            return i;
                    return str.length();
                }
            }
            int lastInd = new H3().findLastNumericalSym(str1);
            String str3 = str1.substring(0, lastInd);

            class H4 {
                int intFromStr(String str) {
                    class H {
                        int isPositive(String str) {
                            char f = str.charAt(0);
                            if (Character.isDigit(f) || f == '+')
                                return 1;
                            else
                                return -1;
                        }
                    }
                    int sign = new H().isPositive(str);

                    //-2147483648 <= x <= 2147483647
                    try {
                        return Integer.valueOf(str);
                    } catch (NumberFormatException n) {
                        if (sign > 0)
                            return Integer.MAX_VALUE;
                        else
                            return Integer.MIN_VALUE;
                    }
                }
            }
            return new H4().intFromStr(str3);
        } else {
            return defaultReturn;
        }
    }

    public static void main(String[] args) {
        System.out.println(myAtoi("   -42"));
    }
}
