package LeetCode.a118

object PascalsTriangle {
  def generate(numRows: Int): List[List[Int]] = {
    val first = List(1)
    val second = List(1, 1)

    def help(res: List[List[Int]] = List(first, second)): List[List[Int]] =
    {
      if (numRows == res.length)
        res
      else {
        help(res :+
          (res(res.length - 1).zipWithIndex.map {
            case (elem: Int, i: Int) =>
              if (i == 0)
                elem
              else
                elem + res(res.length - 1)(i - 1)
          } :+ 1)
        )
      }
    }
    if(numRows == 0)
      Nil
    else if(numRows == 1)
      List(first)
    else
      help()
  }
}
//    def x( list: List[Int]): List[Int] = {
//      list.zipWithIndex.map {
//        case (i, index) =>
//          if(index == 0)
//            i
//          else{
//            i + list(index - 1)
//          }
//      } :+ 1
//    }
//    Stream.iterate(List(1))(x).toList
//  }
//}
