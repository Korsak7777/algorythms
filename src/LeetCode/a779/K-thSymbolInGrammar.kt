package LeetCode.a779

fun kthGrammar(n:Int, k:Int):Int {
    fun getRow(i:Int, n: Int, s:String): String {
        if(i<n)
            return getRow(i+1, n,
                s.map {
                    if (it =='0')
                        "01"
                    else
                        "10"
                }.reduce { acc, s ->  acc+s})
        return s
    }
    val x = getRow(1,n, "0")
    return x[k-1].toString().toInt()
}

fun main(){
    println(kthGrammar(1,1))
}



