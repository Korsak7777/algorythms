package LeetCode.a1047

class RemoveAllAdjacentDuplicatesInString {
    fun removeDuplicates(S: String): String {
        var s = S
        for(i in 0..s.length-1){
            if(s[i] == s[i+1]){
                s = s.replaceRange(i, i+1, "")
                s = removeDuplicates(s)
                break
            }
        }
        return s
    }
}