package LeetCode.a69;

public class Sqrt {
    public static int mySqrt(int x) {
        if(x == 0)
            return 0;
        int i;
        for (i = 1; i < x; i++) {
            if (x - i * i < 0)
                return i-1;
        }
        return 1;
    }

    public static void main(String[] args) {
        mySqrt(1);

        System.out.println((Integer.MAX_VALUE)*2);
    }
}
