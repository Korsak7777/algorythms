package LeetCode.a125;

public class ValidPalindrome {
    public static boolean isPalindrome(String s) {
        int n = s.length();
        if(n == 0)
            return true;

        for(int i = 0, j = n-1; i < n && j >= 0; ){
            char left = s.charAt(i), right = s.charAt(j);
            while (i<n && !Character.isLetterOrDigit(left = s.charAt(i++))){}//"., 0"; "0"

            while (j>=0 && !Character.isLetterOrDigit(right = s.charAt(j--))){}

            left = Character.toLowerCase(left);
            right = Character.toLowerCase(right);

            if(Character.isLetterOrDigit(left) && Character.isLetterOrDigit(right))
                if(left != right)
                    return false;

            if(i > j)
                break;
        }
        return true;
    }

    public static void main(String[] args) {
        isPalindrome("A man, a plan, a canal: Panama");
        isPalindrome(".,");
        isPalindrome("ab");
    }
}
