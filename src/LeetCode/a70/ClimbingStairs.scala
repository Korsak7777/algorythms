package LeetCode.a70

import scala.collection.immutable.HashMap
import scala.collection.mutable


//Input: 3
//Output: 3
//Explanation: There are three ways to climb to the top.
//1. 1 step + 1 step + 1 step
//2. 1 step + 2 steps
//3. 2 steps + 1 step

object ClimbingStairsDown {
  val map = new mutable.HashMap[Int, Int]
  def climbStairs(n: Int): Int = {
    if (n<=0)
      return 1
    if (n == 1)
      return 1
    if(map.contains(n))
      return map.getOrElse(n, 0)

    map.put(n,climbStairs(n-1) + climbStairs(n-2))

    map.getOrElse(n,0)
  }

  def main(args: Array[String]): Unit = {
    print(climbStairs(3))
  }
}
