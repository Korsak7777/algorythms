package LeetCode.a105;


import helpStructure.TreeNode;

public class ConstructBinaryTreeFromPreorderAndInorderTraversal {
    public TreeNode buildTree(int[] preorder, int[] inorder) {
        TreeNode root = new TreeNode(preorder[0]);

        root.left = new TreeNode(preorder[1]);
        root.right = new TreeNode(preorder[2]);

        return new TreeNode(1);
    }
}
