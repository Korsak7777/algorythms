package LeetCode.a224;

public class BasicCalculator {
    public static int calculate(String s) {
        String withoutSpaces = s.replaceAll("\\s+", "");
        return calculateWithoutSpaces(withoutSpaces);
    }

    private static int calculateWithoutSpaces(String s) {
        char[] c = s.toCharArray();
        int bracketCount = 0;
        int endIndex = -1;
        int i = c.length - 1;
        while (i >= 0) {
            if (c[i] == ')') {
                bracketCount++;
                if (endIndex == -1)
                    endIndex = i;
            } else if (c[i] == '(') {
                bracketCount--;
                if (bracketCount == 0) {
                    if (i == 0) { //  || s.substring(0,i).trim().isEmpty()
                        return calculate(s.substring(i + 1, endIndex));
                    } else if (c[i - 1] == '+') {
                        return calculate(s.substring(0, i - 1)) + calculate(s.substring(i + 1, endIndex));
                    } else if (c[i - 1] == '-') {
                        return calculate(s.substring(0, i - 1)) - calculate(s.substring(i + 1, endIndex));
                    } else if (c[i - 1] == ' ') {
                        continue;
                    }
                    endIndex = -1;
                }
            } else if (c[i] == '+' && endIndex == -1) {
                return calculate(s.substring(0, i)) + calculate(s.substring(i + 1));
            } else if (c[i] == '-' && endIndex == -1) {
                return calculate(s.substring(0, i)) - calculate(s.substring(i + 1));
            }
            i--;
        }
        return Integer.parseInt(s);
    }

    public static void main(String[] x){
        System.out.println(calculate(" 2-1 + 2 ")); //1

        System.out.println(calculate("(1-(4+5+2)-3)+(6+8)")); //1

        System.out.println("(  3)");
    }
}
