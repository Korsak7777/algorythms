package LeetCode.a36;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ValidSudoku {
    public boolean isValidSudoku(char[][] board) {
        ArrayList<HashSet<Character>> lines = new ArrayList<>(9);
        ArrayList<HashSet<Character>> columns = new ArrayList<>(9);
        for (int i = 0; i < 9; i++){
            lines.add(new HashSet<Character>());
            columns.add(new HashSet<Character>());
        }
        ArrayList<ArrayList<HashSet<Character>>> squares = new ArrayList<>(3);
        for (int i = 0; i < 3; i++) {
            squares.add(new ArrayList<HashSet<Character>>(3));
            for (int j = 0; j < 3; j++) {
                squares.get(i).add(new HashSet<Character>());
            }
        }

        int i = 0;
        for (char[] line : board) {
            int j = 0;

            for (char e : line) {
                if(e != '.'){
                    HashSet lineSet = lines.get(i);
                    HashSet columnSet = columns.get(j);
                    HashSet squareSet = squares.get(i/3).get(j/3);

                    if (lineSet.add(e)
                            && columnSet.add(e)
                            && squareSet.add(e)){

                    } else
                        return false;
                }
                j++;
            }
            i++;
        }
        return true;
    }
}
