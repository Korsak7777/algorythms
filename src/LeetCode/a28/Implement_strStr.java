package LeetCode.a28;

public class Implement_strStr {
    public static int strStr(String haystack, String needle) {
        if(needle.isEmpty())
            return 0;

        for(int hI = 0; hI + needle.length() <= haystack.length(); hI++){
            if(needle.charAt(0) == haystack.charAt(hI)){
                if(haystack.substring(hI, hI + needle.length()).equals(needle))
                    return hI;
            }
        }
        return -1;
    }

    public static void main(String[] args) {
        strStr("aaa", "aaaa");
    }
}
