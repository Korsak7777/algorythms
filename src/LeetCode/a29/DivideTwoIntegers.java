package LeetCode.a29;

public class DivideTwoIntegers {
    public int divide(int dividend, int divisor) {
        if(dividend == 0)
            return 0;


        boolean isNegative = false;
        if(dividend < 0){
            dividend = dividend * -1;
            isNegative = !isNegative;
        }
        if(divisor < 0){
            divisor = divisor * -1;
            isNegative = !isNegative;
        }

        int res = 0;
        int i = 0;
        while (res < dividend - divisor){
            i++;
            res += divisor;
        }

        if(isNegative)
            return i * -1;
        else
            return i;
    }
}
