package LeetCode.a94;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class BinaryTreeInorderTraversalRecursive {

    private List<Integer> l = new LinkedList<>();
    public List<Integer> inorderTraversal(TreeNode root) {
        inorderTraversal1(root);
        return l;
    }

    private void inorderTraversal1(TreeNode root) {
        if (root != null) {
            inorderTraversal(root.left);
            visit(root);
            inorderTraversal(root.right);
        }
    }

    private void visit(TreeNode root){
        l.add(root.val);
    }

    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;

        TreeNode(int x) {
            val = x;
        }
    }
}
