package LeetCode

class Solution1 {
    fun isPalindrome(x: Int): Boolean {
        var result = false
        val sn = x.toString().toCharArray()
        val l = x.toString().length
        if (l<2) return true
        val mid = if(l%2 == 0) l/2 else l/2 +1

        for( i in 0..mid){
            if(sn[l-1-i] != sn[i]) {
                return false
            }
            result = true
        }
        return result

    }
}

fun main() {
    Solution1().isPalindrome(1000021)
}