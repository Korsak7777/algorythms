package LeetCode.a278



object FirstBadVersion {
  def isBadVersion(version: Int): Boolean = {
    if (version >= 1702766719)
      true
    else
      false
  }

  def firstBadVersion(n: Int): Int = {
    val start = 1
    val finish = n
    def firstBadVersion(start:Int, finish: Int): Int ={
      if(start == finish)
        return start
      val mid = start + (finish - start)/2
      if(isBadVersion(mid))
        firstBadVersion(start, mid)
      else
        firstBadVersion(mid+1, finish)
    }
    firstBadVersion(start, finish)
  }

  def main(args: Array[String]): Unit = {
    print(firstBadVersion(2126753390))
  }
}
