package LeetCode.a83

import helpStructure.scala.ListNode


//Input: 1->1->2->3->3
//Output: 1->2->3

object RemoveDuplicatesFromSortedList {

  def deleteDuplicates(_head: ListNode): ListNode = {
    def deleteDuplicates(head: ListNode): ListNode = {
      if(head == null || head.next == null)
        return _head
      if(head.x == head.next.x) {
        if(head.next.next == null){
          head.next = null
          return _head
        }
        head.next = head.next.next
        deleteDuplicates(head)
      }
      deleteDuplicates(head.next)
    }
    deleteDuplicates(_head)
  }


  def main(args: Array[String]): Unit = {
    val x1 = new ListNode(1)
    x1.next = new ListNode(3)
    x1.next.next = new ListNode(4)
    x1.next.next.next = new ListNode(4)
    x1.next.next.next.next = new ListNode(4)
    x1.next.next.next.next.next = new ListNode(5)
    x1.next.next.next.next.next.next = new ListNode(5)

    print(deleteDuplicates(x1))
  }
}
