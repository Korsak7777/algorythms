package LeetCode.a66;

public class PlusOne {
    public int[] plusOne(int[] digits) {
        int lastDigit = digits[digits.length-1];
        if(lastDigit != 9){
            digits[digits.length-1]++;
            return digits;
        } else {
            digits[digits.length-1] = 0;
            for(int i = digits.length-2; i >= 0; i--){
                if(digits[i] != 9){
                    digits[i]++;
                    return digits;
                } else
                    digits[i] = 0;
            }
        }

        int[] res = new int[digits.length + 1]; // this case for 999. We create 0000
        res[0] = 1; //do 1000
        return res;
    }
}
