package LeetCode.a189;

import sun.security.util.ArrayUtil;

import java.util.Arrays;

public class RotateArray {
//    nums = [1,2,3,4,5,6,7], k = 3
//    Output: [5,6,7,1,2,3,4]
    public static void rotate(int[] nums, int k) {
        k = k % nums.length;
        int[] buf = Arrays.copyOfRange(nums, nums.length - k, nums.length);
        System.arraycopy(nums, 0, nums, k, nums.length-k);
        System.arraycopy(buf, 0, nums, 0, buf.length);
    }

    public static void main(String[] args) {
        System.out.println(1%2);
    }
}
