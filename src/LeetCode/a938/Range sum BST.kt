package LeetCode.a938

class TreeNode(var `val`:Int){
    var left:TreeNode? = null
    var right:TreeNode? = null
}

class Solution {
    var sum = 0
    fun rangeSumBST(root: TreeNode?, L: Int, R: Int): Int {
        if(root != null){
            if (root.`val` in L..R)
                sum += root.`val`
            if (root.left != null && root.`val`<R)
                rangeSumBST(root.left, L, R)
            if (root.right != null && root.`val`>L)
                rangeSumBST(root.right, L, R)
        }
        return sum
    }
}

fun main(){
    if (1 in 1..11)
        print(1)
}