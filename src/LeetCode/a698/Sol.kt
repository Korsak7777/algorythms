package LeetCode.a698

class Solution {
    private var sas:Int = 0
    fun canPartitionKSubsets(elem: IntArray, k: Int): Boolean {
        val fsElem = elem.toList().filter { it!=0 }.sorted().toMutableList()
        val sum = fsElem.reduce { i, j ->  i + j}
        sas = sum/k
        if (sum % k > 0 && fsElem.last()>sas)
            return false
        return rec(sas, sum, fsElem)
    }

    private fun rec(limit1: Int, sum: Int, fsElem: MutableList<Int>): Boolean {
        val limit = if (limit1 == 0) sas else limit1
        return if (sum == 0 && fsElem.isEmpty())
            true
        else {
            val closestIndex = fsElem.findClose(limit)
            if (closestIndex == null)
                false
            else {
                val closest = fsElem.removeAt(closestIndex)
                rec(limit-closest, sum - closest, fsElem)
            }
        }
    }
}

/**
 * @return index of sorted list closest element to limit that < limit
 */
private fun List<Int>.findClose(limit: Int): Int? {
    if (this.isEmpty())
        return null
    return if (this.last()<=limit)
        this.size - 1
    else
        this.dropLast(1).findClose(limit)
}

fun main(){
    print(Solution().canPartitionKSubsets(intArrayOf(10,10,10,7,7,7,7,7,7,6,6,6), 3))
}

