package LeetCode.a15;

import java.util.*;

public class ThreeSum {
    public List<List<Integer>> threeSum(int[] nums) {
        if (nums.length <= 3)
            return new ArrayList<>();
        class Duplet{
            int a;
            int b;

            Duplet(int a, int b){
                this.a = a;
                this.b = b;
            }
            @Override
            public boolean equals(Object o) {
                if (this == o) return true;
                if (o == null || getClass() != o.getClass()) return false;

                Duplet duplet = (Duplet) o;

                if (a != duplet.a) return false;
                return b == duplet.b;
            }

            @Override
            public int hashCode() {
                int result = a;
                result = 31 * result + b;
                return result;
            }

            public boolean contains(int cI) {
                if(a == cI || b == cI)
                    return true;
                else
                    return false;
            }
        }

        Map<Integer, Set<Duplet>> sumMap = new HashMap();
        for (int aI = 0; aI < nums.length; aI++) {
            int a = nums[aI];
            for (int bI = aI + 1; bI < nums.length; bI++) {
                int b = nums[bI];
                int sum = a + b;
                if (sumMap.containsKey(sum)) {
                    Duplet duplet = new Duplet(aI, bI);
                    sumMap.get(sum).add(duplet);
                } else {
                    Set s = new HashSet();
                    s.add(new int[]{aI, bI});
                    sumMap.put(sum, s);
                }
            }

        }
        List<List<Integer>> res = new ArrayList<>();
        for (int cI = 0; cI < nums.length; cI++) {
            int c = nums[cI];
            if(sumMap.containsKey(c)){
                Set<Duplet> duplets = sumMap.get(c);
                for (Duplet duplet : duplets){
                    if (duplet.contains(cI))
                        continue;
                    List<Integer> triplet = new ArrayList<>();
                    triplet.add(c);
                    triplet.add(duplet.a);
                    triplet.add(duplet.b);
                    res.add(triplet);
                }
            }
        }

        return res;
    }
}
