package LeetCode.a726
//class Solution{
    class TreeNode(val value:MutableMap<String, Int>, var multiplier:Int){
        val childs: MutableList<TreeNode> = mutableListOf()
    }
    //multiplier on the end of formula, so it will be logic start from the end
    fun countOfAtoms(formulaH: String): String {
        val formula = "($formulaH)1"

        fun parseElement(subSequence: CharSequence): Pair<String, Int> {
            var element = ""
            var count = ""
            subSequence.forEach {
                if (it.isLetter())
                    element += it
                if (it.isDigit())
                    count += it
            }
            return Pair(element, if(count.isEmpty()) 1 else count.toInt())
        }
        fun MutableMap<String, Int>.addElem(elements:Map<String, Int>): MutableMap<String, Int> {
            elements.forEach { (k, v) ->
                if(this.contains(k))
                    this[k] = this[k]!! + v
                else
                    this[k] = v
            }
            return this
        }
        fun getElement(index:Int): MutableMap<String, Int> {
            var j = index + 1
            while (formula[j].isLowerCase() || formula[j].isDigit())
                j++
            val elem = parseElement(formula.subSequence(index,j))
            return mutableMapOf(elem.first to elem.second)
        }
        fun getMultiplier(index:Int):Int {
            var j = index+1
            while (j<formula.length && formula[j].isDigit())
                j++
            return formula.subSequence(index+1,j).toString().toInt()
        }

        fun getTreeOfElements(x:Pair<TreeNode, Int>): Pair<TreeNode, Int> {
            val tree = x.first
            var i = x.second
            while (i<formula.length){
                when {
                    formula[i] == '(' -> {
                        val x = getTreeOfElements(tree to ++i)
                        tree.childs.add(x.first)
                        i = x.second
                    }
                    formula[i].isUpperCase() -> {
                        tree.value.addElem(getElement(i))
                    }
                    formula[i] == ')' -> {
                        tree.multiplier = getMultiplier(i)
                        return tree to ++i
                    }
                }
                i++
            }
            return tree to i
        }

        fun getMapOfElements(tree: TreeNode): Map<String, Int> {
            var result = mutableMapOf<String, Int>()
            result.addElem(tree.value)
            for (child in tree.childs){
                result.addElem(child.value)
                result.addElem(getMapOfElements(child))
            }
            return result
        }

        return getMapOfElements(
                getTreeOfElements(TreeNode(mutableMapOf(), 1) to 0).first)
                .map {
                    "${it.key}${if(it.value == 1) "" else it.value}"
                }.sorted().reduce { acc, s ->  acc+s}
    }
//}

fun main() {
    println(countOfAtoms("H2O"))

    println(countOfAtoms("Mg(OH)2"))

    println(countOfAtoms("K4(ON(SO3)2)2"))
}