package LeetCode.a783;

import java.util.TreeSet;

public class MinimumDistanceBetweenBST {
    class TreeNode {
        int val;
        TreeNode left;
        TreeNode right;
        TreeNode(int x) { val = x; }
    }

    class Solution {
        int minDiff = Integer.MAX_VALUE;

        int minDiffInBST(TreeNode root) {
            if(root != null){
                minDiffInBST(root.left);

                

                minDiffInBST(root.right);
                return minDiff;
            }
            return 0;
        }
    }

}

