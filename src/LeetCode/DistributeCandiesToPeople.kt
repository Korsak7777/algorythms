package LeetCode

class Solution {
    fun distributeCandies(c: Int, n: Int): IntArray {
        var menNow = 0
        var confNow = 1
        var confLast = c
        val ans = IntArray(n){0}
        while (confLast > confNow){
            ans[menNow] += confNow
            confLast -= confNow
            confNow++
            if(menNow < n) menNow++
            else menNow = 0
        }
        ans[menNow] += confLast
        return ans
    }
}