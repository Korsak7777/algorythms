package LeetCode.a41;

//        Input: [1,2,0]
//        Output: 3
//
//        Input: [3,4,2,1]
//        Output: 2
//
//        Input: [7,8,9,11,12]
//        Output: 1

public class FirstMissingPositive {
    public int firstMissingPositive(int[] nums) {
        if(nums.length == 0)
            return 1;

        int min = 1;
        int[] x = new int[nums.length];
        for(int i = 0; i < nums.length; i++){
            int num = nums[i];
            if(num <= nums.length && num > 0){
                x[num-1] = num;
            }
        }
        for(int i = 0; i < nums.length; i++){
            if (x[i] == 0){
                return i+1;
            }
        }
        return nums.length+1;
    }
}
