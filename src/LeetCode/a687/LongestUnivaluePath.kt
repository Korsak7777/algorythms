package LeetCode.a687

import helpStructure.kotlin.TreeNode


class Solution{
    fun longestUnivaluePath(root: TreeNode?):Int{
        var ans:Int = 0

        /**
         *
         * @return length of longest arrow in tree
         */
        fun arrow(node: TreeNode?):Int{
            if(node == null) return 0
            val arrowLeft = arrow(node.left)
            val arrowRight = arrow(node.right)

            val left = if(node.left != null && node.left!!.`val` == node.`val`)
                arrowLeft + 1
            else 0
            val right = if(node.right != null && node.right!!.`val` == node.`val`)
                arrowRight + 1
            else 0

            ans = Math.max(ans, left + right)
            return Math.max(left, right)
        }
        arrow(root)
        return ans
    }
}