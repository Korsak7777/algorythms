package LeetCode.a21

import helpStructure.scala.ListNode

object MergeTwoSortedLists {
  def mergeTwoLists(l1: ListNode, l2: ListNode): ListNode = {
    var res: ListNode = null
    if (l1 == null && l2 == null)
      return res
    if (l1 == null)
      return l2
    if (l2 == null)
      return l1

    if(l1.x <= l2.x) {
      res = new ListNode(l1.x)
      res.next = mergeTwoLists(l1.next,l2)
    } else {
      res = new ListNode(l2.x)
      res.next = mergeTwoLists(l1,l2.next)
    }
    res
  }

  //5
  //1 -> 3 -> 4
  //1 -> 3 -> 4 -> 5

  //1 -> 3 -> 4
  //1 -> 2 -> 3
  //1 -> 1 -> 2 -> 3 -> 3 -> 4

  def main (args: Array[String] ): Unit = {
    val x1 = new ListNode(1)
    x1.next = new ListNode(3)
    x1.next.next = new ListNode(4)
    val x2 = new ListNode(5)

    print(mergeTwoLists(x1, x2))
  }
}



