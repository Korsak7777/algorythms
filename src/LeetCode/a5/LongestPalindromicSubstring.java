package LeetCode.a5;

import java.util.*;

//        Input: "babad"
//        Output: "bab"
//        Note: "aba" is also a valid answer.
//        Example 2:
//
//        Input: "cbbd"
//        Output: "bb"

public class LongestPalindromicSubstring {
    //start and back palindrome index                       //   012345
    public String longestPalindrome(String s) {             //  "abacab"
        if(s.isEmpty())
            return "";
        int[] longestPalindrome = new int[2];
        int longestLength = Integer.MIN_VALUE;

        for(int i = 0; i < s.length(); i++){
            if(longestLength >= s.length()-i)
                break;
            char startCurr = s.charAt(i);
            for (int j = s.length()-1; j > i; j--){
                if(longestPalindrome[1] - longestPalindrome[0] >= j-i)
                    break;

                char backCurr = s.charAt(j);
                if(startCurr == backCurr) {
                    if(isPalindrome(s.substring(i,j+1))){
                        longestPalindrome[0] = i;
                        longestPalindrome[1] = j;
                        longestLength = j-i;
                        break;
                    }
                }
            }
        }
        return s.substring(longestPalindrome[0], longestPalindrome[1]+1);
    }

    private boolean isPalindrome(String s){
        int mid = s.length()/2;

        int frontIt = 0;
        int lastIt = s.length()-1;

        int i = 0;
        while (i++ < mid){
            if (s.charAt(frontIt++) != s.charAt(lastIt--))
                return false;
        }
        return true;
    }

    private <T> boolean isPalindrome(LinkedList<T> l) {
        int mid = l.size() / 2 + 1;
        Iterator<T> frontIt = l.iterator();
        Iterator<T> lastIt = l.descendingIterator();

        int i = 0;
        while (i <= mid){
            if (!frontIt.next().equals(lastIt.next()))
                return false;
            i++;
        }
        return true;
    }
}
