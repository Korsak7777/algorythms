package LeetCode.a4;

//        nums1 = [1, 3]
//        nums2 = [2]
//        The median is 2.0
//
//        nums1 = [1, 2]
//        nums2 = [3, 4]
//        The median is (2 + 3)/2 = 2.5

public class MedianOfTwoSortedArrays {
    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        final int[] x = new int[nums1.length + nums2.length];
        int indexX = 0;
        int index1 = 0;
        int index2 = 0;
        while (indexX < x.length){
            if(index1 == nums1.length){
                System.arraycopy(nums2, index2, x, indexX, nums2.length - index2);
                break;
            } else if (index2 == nums2.length){
                System.arraycopy(nums1, index1, x, indexX, nums1.length - index1);
                break;
            }

            if(nums1[index1] <= nums2[index2]){
                x[indexX] = nums1[index1];
                index1++;
            } else {
                x[indexX] = nums2[index2];
                index2++;
            }
            indexX++;
        }

        return findMedianSortedArray(x);
    }

    private double findMedianSortedArray(int[] num){
        if(num.length % 2 == 1)
            return num[num.length/2];
        else
            return num[num.length/2] + (num[num.length/2-1] - num[num.length/2])/2.;
    }
}
