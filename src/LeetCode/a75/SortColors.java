package LeetCode.a75;

public class SortColors {
//    Input: [2,0,2,1,1,0]
//    Output: [0,0,1,1,2,2]
//
    public static void sortColors(int[] nums) {
        int startIndex = 0;
        int endIndex = nums.length - 1;
        int temp,i =0;
        while(i < nums.length) {
            if (nums[i] == 0 && i != startIndex) {
                temp = nums[i];
                nums[i] = nums[startIndex];
                nums[startIndex++] = temp;
            } else if (nums[i] == 2 && i < endIndex) {
                temp = nums[i];
                nums[i] = nums[endIndex];
                nums[endIndex--] = temp;
            } else {
                i++;
            }
        }
    }

    public static void main(String[] args) {
        sortColors(new int[]{2,1,0,2,1,1,0});
    }
}
