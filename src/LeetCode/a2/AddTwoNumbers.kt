package LeetCode.a2

class ListNode(var `val`: Int) {
    var next: ListNode? = null
}

fun addTwoNumbers(l1: ListNode?, l2: ListNode?): ListNode? {
    var x : ListNode? = ListNode(0);

    var p1: ListNode? = l1
    var p2: ListNode? = l2

    var result = x

    var plusOne = 0

    while (p1 != null || p2 != null){
        var res: Int = (p1?.`val` ?: 0) + (p2?.`val` ?: 0) + plusOne
        plusOne = res / 10
        res %= 10

        result!!.next = ListNode(res)
        result = result!!.next

        if(p1!=null) p1 = p1.next
        if(p2!=null) p2 = p2.next
    }

    if(plusOne>0) result!!.next = ListNode(1)

    return x!!.next
}

fun main(args: Array<String>) {
    val l1 = ListNode(4)
    l1.next = ListNode(3)

    val l2 = ListNode(1)
    l2.next = ListNode(2)

    print(addTwoNumbers(l1, l2))
}