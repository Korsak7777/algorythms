package LeetCode.a2;

//Input: (2 -> 4 -> 3) + (5 -> 6 -> 4)
//        Output: 7 -> 0 -> 8
//        Explanation: 342 + 465 = 807.


public class AddTwoNumbers {

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode res = new ListNode(0);
        addTwoNumbers(l1, l2, res, 0);
        res = res.next;
        return res;
    }

    private ListNode addTwoNumbers(ListNode l1, ListNode l2, ListNode res, int extraOne){
        if (l1 == null && l2 == null){
            int next = sum(l1, l2, extraOne);
            if(next > 0)
                res.next = new ListNode(sum(l1, l2, extraOne));
            return res;
        }
        int next = sum(l1, l2, extraOne);
        int extra = next / 10;
        next = next % 10;

        res.next = new ListNode(next);

        if (l1 == null)
            return addTwoNumbers(l2.next, null, res.next, extra);
        else if(l2 == null)
            return addTwoNumbers(l1.next, null, res.next, extra);
        else
            return addTwoNumbers(l1.next, l2.next, res.next, extra);
    }

    private int sum(ListNode l1, ListNode l2, int extra){
        int x1, x2;
        if(l1 == null)
            x1 = 0;
        else
            x1 = l1.val;
        if(l2 == null)
            x2 = 0;
        else
            x2 = l2.val;
        return x1 + x2 + extra;
    }


    class ListNode {
        int val;
        ListNode next;

        ListNode(int x) {
            val = x;
        }
    }
}
