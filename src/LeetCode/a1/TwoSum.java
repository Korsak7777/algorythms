package LeetCode.a1;

//Given nums = [2, 7, 11, 15], target = 9,  22
//
//        Because nums[0] + nums[1] = 2 + 7 = 9,
//        return [0, 1].

//[0,4,3,0]     0

//[-1,-2,-3,-4,-5]  -8

public class TwoSum {
    public static int[] twoSum(int[] nums, int target) {
        int i = 0;
        for(int num :nums) {
            int desired = target - num;
            for(int j = i + 1; j < nums.length; j++){
                if(nums[j] == desired)
                    return new int[]{i,j};
            }
            i++;
        }
        return new int[2];
    }

    public static void main(String[] args) {
        twoSum(new int[]{0,4,3,0}, 0);
        System.out.println(0-0);
    }
}
