package LeetCode.a11;

public class ContainerWithMostWater {
//    Input: [1,8,6]
//    Output: 49
    public int maxArea(int[] height) {
        int max = Integer.MIN_VALUE;
        for(int i = 0; i < height.length-1; i++){
            int h1 = height[i];
            for (int j = i+1; j < height.length; j++){
                int l = j - i;
                int h2 = height[j];
                int h = Integer.min(h1, h2);
                max = Integer.max(h * l, max);
            }
        }
        return max;
    }
}
