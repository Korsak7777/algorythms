package LeetCode.a300;

import java.util.Arrays;
import java.util.HashMap;

public class LongestIncreasingSubsequence {
    public static int lengthOfLIS(int[] nums) {
        int[] dp = new int[nums.length];
        int max = 0;
        for(int num : nums) {
            int index = Arrays.binarySearch(dp, 0, max, num);
            if(index < 0) index = Math.abs(index) - 1;
            dp[index] = num;
            if(index == max) ++max;
        }

        return max;
    }
    static HashMap<Integer, Integer> map = new HashMap();
    private static int findNext(int[] nums, int j) {
        if(map.containsKey(j))
            return map.get(j);
        for(int i = j+1; i<nums.length; i++){
            if(nums[j] < nums[i]){
                map.put(j, i);
                return i;
            }
        }
        map.put(j, -1);
        return -1;
    }
}
