package LeetCode.a14;

public class LongestCommonPrefix {
    public String longestCommonPrefix(String[] strs) {
        if(strs.length == 0)
            return "";
        if(strs.length == 1)
            return strs[0];

        StringBuilder prefix = new StringBuilder();

        class H {
            private boolean isEnd(int charNum){
                if(strs[0].isEmpty() || charNum >= strs[0].length())
                    return true;
                char c = strs[0].charAt(charNum);
                for (int wordNum = 1; wordNum<strs.length; wordNum++){
                    String word = strs[wordNum];
                    if(word.isEmpty() || charNum >= word.length())
                        return true;
                    if(word.charAt(charNum) != c)
                        return true;
                }
                prefix.append(c);
                return false;
            }
        }

        int charNum = 0;
        while (!new H().isEnd(charNum))
            charNum++;

        return prefix.toString();
    }
}
