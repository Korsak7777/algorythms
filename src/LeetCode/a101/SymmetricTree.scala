package LeetCode.a101

import helpStructure.scala.TreeNode

object SymmetricTree {
  def isSymmetric(root: TreeNode): Boolean = {
    def isMirror(left: TreeNode, right: TreeNode):Boolean = {
      if(left == null && right == null)
        return true
      else if (left == null || right == null)
        return false
      left.value == right.value && isMirror(left.left, right.right) && isMirror(left.right, right.left)
    }
    root == null || isMirror(root.left, root.right)
  }

  def main(args: Array[String]): Unit = {
    val root = new TreeNode(2)
    root.left = new TreeNode(3)
    root.right = new TreeNode(3)
    print(isSymmetric(root))
  }

}
