package book.insertionSort2_1

class InsertionSort(val a: IntArray) {
    fun sortASC() {
        for (j in 1 until a.size){
            var key = a[j]
            var i = j -1
            while(i>=0 && a[i] > key){
                a[i+1] = a[i]
                i--
            }
            a[i+1] = key
        }
    }

    fun sortDESK() { //2.1.2
        for (j in 1 until a.size){
            var key = a[j]
            var i = j -1
            while(i>=0 && a[i] < key){
                a[i+1] = a[i]
                i--
            }
            a[i+1] = key
        }
    }

}

fun main(){
    val IS = InsertionSort(intArrayOf(5,2,4,6,1,3))
    IS.sortASC()
    IS.sortDESK()
    print(IS.a.asList())
}