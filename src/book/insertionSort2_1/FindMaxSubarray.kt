package book.insertionSort2_1

fun main() {
    val A = intArrayOf(13,-3,-25,20,-3,-16,-23,18,20,-7,12,-5,-22,15,-4,7)
}

fun IntArray.findMaxSubarray(): Map<String, Int> {
    fun findMaxCrossingSubArray(A: IntArray, low: Int, mid: Int, high: Int): Map<String, Int> {
        var leftSum = Int.MIN_VALUE
        var sum = 0
        var maxLeft:Int = low
        for (i in mid..low){
            sum += A[i]
            if (sum > leftSum){
                leftSum = sum
                maxLeft = i
            }
        }

        var rightSum = Int.MIN_VALUE
        sum = 0
        var maxRight:Int = high
        for (j in mid+1 until high){
            sum += A[j]
            if (sum > rightSum){
                rightSum = sum
                maxRight = j
            }
        }
        return mapOf(
            "maxLeft" to maxLeft,
            "maxRight" to maxRight,
            "sum" to (leftSum - rightSum))
    }

    fun findMaxSubarray(A: IntArray, low: Int, high: Int):Map<String, Int>{
        if (high == low){
            return mapOf("maxLeft" to low,
                "maxRight" to high,
                "sum" to A[low])
        } else {
            var mid = (low + high)/2
            val left = findMaxSubarray(A, low, mid)
            val right = findMaxSubarray(A, mid + 1, high)
            val cross = findMaxCrossingSubArray(A, low, mid, high)
            if(left["sum"]!! >= right["sum"]!! && left["sum"]!! >= right["sum"]!!){}
            //    return
            //  and more and more...
            //page 97
            return mapOf()
        }
    }
    return findMaxSubarray(this, 0, this.size - 1);
}

