package book.insertionSort2_1

import kotlin.random.Random

fun IntArray.insertionSort(){
    for (j in 1 until this.size){
        var key = this[j]
        var i = j -1
        while(i>=0 && this[i] > key){
            this[i+1] = this[i]
            i--
        }
        this[i+1] = key
    }
}

fun IntArray.mergeSort(){
    fun mergeSort(start:Int, finish:Int){
        fun merge(A:IntArray, start:Int, mid:Int, finish:Int){
            val left0 = mid - start +1
            val right0 = finish - mid
            val L = IntArray(left0+1){A.elementAtOrElse(start + it){ Int.MAX_VALUE}}
            val R = IntArray(right0 + 1){A.elementAtOrElse(mid + it + 1){ Int.MAX_VALUE}}

            L[left0] = Int.MAX_VALUE
            R[right0] = Int.MAX_VALUE

            var i = 0
            var j = 0
            for (k in start..finish){
                if(k < A.size){
                    if(L[i] <= R[j]) A[k] = L[i++]
                    else A[k] = R[j++]
                }

            }
        }

        if (start < finish){
            val mid = (start + finish)/2 //find middle
            mergeSort(start, mid)
            mergeSort(mid+1, finish)
            merge(this, start, mid, finish)
        }
    }
    return mergeSort(0, this.size)
}

fun main(){
    var a = IntArray(50){Random.nextInt()/10000000}
    //a = intArrayOf(3,2,4,6,7)
    val target = IntArray(a.size){a[it]}.toList().sorted()
    a.mergeSort()
    println(a.toList())
    println(target)
}