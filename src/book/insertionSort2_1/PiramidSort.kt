package book.insertionSort2_1

class Pyramid(val IntArray:IntArray){
    var length = IntArray.size
    var heapSize = 0
}

fun heapSort(P:Pyramid){
    val A = P.IntArray
    buildMaxHeap(P)
    for (i in P.length-1..1){
        val x = A[0]
        A[0] = A[i]
        A[i] = x

        P.heapSize--
        maxHeapify(P, 0)
    }
}

private fun buildMaxHeap(P:Pyramid){
    P.heapSize = P.length
    for(i in P.length/2..1)
        maxHeapify(P,i)
}

private fun maxHeapify(P:Pyramid, i:Int){
    val A = P.IntArray
    val l = left(i)
    val r = right(i)
    val largest = if(l<=P.heapSize && A[l] > A[i]) l
                    else i
                    if(r<=P.heapSize && A[r] > A[largest]) r
    if(largest != i){
        val x = A[largest]
        A[largest] = A[i]
        A[i] = x
        maxHeapify(P, largest)
    }
}

private fun left(i:Int): Int{
    return 2*i
}
private fun right(i:Int): Int{
    return 2*i + 1
}
private fun parent(i:Int): Int{
    return i/2
}