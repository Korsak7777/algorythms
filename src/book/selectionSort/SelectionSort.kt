package book.selectionSort

class SelectionSort(val a: IntArray) {
    fun sort() {
        for (j in 0 until a.size){
            var min = j
            val el = a[j]
            for(i in j+1 until a.size) {
                if(a[min] > a[i])
                    min = i
            }
            a[j] = a[min]
            a[min] = el
        }
    }
}

fun main(){
    val IS = SelectionSort(intArrayOf(5,2,4,6,1,3))
    IS.sort()
    print(IS.a.asList())
}