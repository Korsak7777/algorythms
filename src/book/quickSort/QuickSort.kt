package book.quickSort

fun quickSort(A:IntArray, start:Int, finish:Int) {
    fun swap(a: IntArray, i: Int, j: Int) {
        if(i == j) return
        val x = a[i]
        a[i] = a[j]
        a[j] = x
    }

    fun partition(A:IntArray, start:Int, finish:Int):Int{
        val x = A[finish]
        var i = start - 1
        for(j in start until finish){
            if (A[j] <= x){
                i++
                swap(A, i, j)
            }
        }
        swap(A, i+1, finish)
        return i + 1
    }

    if (start < finish) {
        val mid = partition(A, start, finish)
        quickSort(A, start, mid - 1)
        quickSort(A, mid + 1, finish)
    }
}

fun main(){
    val A = intArrayOf(2,5,8,3,1,5,6)
    quickSort(A, 0, A.size-1)
    print(A.toList())
}