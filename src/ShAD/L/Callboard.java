package ShAD.L;

import java.awt.geom.Point2D;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Callboard {
    static class Point2D implements Comparable{
        int x;
        int y;

        Point2D(int x, int y){
            this.x = x;
            this.y = y;
        }

        public boolean isNeighborhood(Point2D p){
            if(p.x - this.x == 1 || p.y - this.y == 1)
                return true;
            else
                return false;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Point2D point2D = (Point2D) o;

            if (x != point2D.x) return false;
            return y == point2D.y;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        @Override
        public int compareTo(Object o) {
            Point2D p = (Point2D) o;
            if(p.x < this.x)
                return -1;
            else if (p.y < this.y)
                return -1;
            else
                return 1;
        }
    }

    static class Data {
        int W;
        int H;
        int a;
        int minB;
        int n;
        HashMap<Point2D, Point2D> announcementsCord = new HashMap();
        HashMap<Integer, List<Integer>> announcementsPoints = new HashMap<Integer, List<Integer>> ();
        HashSet<Point2D> allBusyPoints = new HashSet<>();

        String path;

        Data(String path){
            this.path = path;
            readData();
        }

        private void readData() {
            try(BufferedReader br = new BufferedReader(new FileReader(path))) {
                String line = br.readLine();

                int i = 0;
                while (line != null) {
                    if(i == 0){
                        ArrayList<Integer> data = new ArrayList<String>(Arrays.asList(line.split(" ")))
                                .stream().map(Integer::valueOf)
                                .collect(Collectors.toCollection(ArrayList::new));
                        W = data.get(0);
                        H = data.get(1);
                        a = data.get(2);
                        minB = data.get(3);
                    } else if (i == 1){
                        n = Integer.valueOf(line);
                    } else {
                        ArrayList<Integer> data = new ArrayList<String>(Arrays.asList(line.split(" ")))
                                .stream().map(Integer::valueOf)
                                .collect(Collectors.toCollection(ArrayList::new));
                        Point2D leftDown =
                                new Point2D(data.get(0), data.get(1));
                        Point2D rightUp =
                                new Point2D(data.get(2), data.get(3));

                        announcementsCord.put(leftDown, rightUp);

                        if(announcementsPoints.containsKey(data.get(0))){
                            announcementsPoints.get(data.get(0)).add(data.get(1));
                        } else {
                            List<Integer> x = new ArrayList<>();
                            x.add(data.get(1));
                            announcementsPoints.put(data.get(0), x);
                        }

                        if(announcementsPoints.containsKey(data.get(2))){
                            announcementsPoints.get(data.get(2)).add(data.get(3));
                        } else {
                            List<Integer> x = new ArrayList<>();
                            x.add(data.get(3));
                            announcementsPoints.put(data.get(2), x);
                        }

                        allBusyPoints = getBusy(data.get(0), data.get(1), data.get(2), data.get(3));
                    }
                    line = br.readLine();
                    i++;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        public static HashSet getBusy(int d0, int d1, int d2, int d3){
            HashSet<Point2D> allBusyPoints = new HashSet<>();

            for(int i = d0; i <= d2; i++){
                for (int j = d1; j <= d3; j++){
                    allBusyPoints.add(new Point2D(i, j));
                }
            }
            return allBusyPoints;
        }

        @Override
        public String toString() {
            return "Data{" +
                    "W=" + W +
                    ", H=" + H +
                    ", a=" + a +
                    ", minB=" + minB +
                    ", n=" + n +
                    ", announcementsCoord=" + announcementsCord +
                    ", path='" + path + '\'' +
                    '}';
        }
    }

    static class FreeArea{
        TreeSet<Point2D> points = new TreeSet<>();
    }

    public static void main(String[] args) {
        Data data = new Data("input.txt");

        HashMap<Point2D, Point2D> announcementsCord = data.announcementsCord;

        HashMap<Point2D, Point2D> answerCord = new HashMap<>();

        HashSet<Point2D> allPossiblePoint = Data.getBusy(0, 0, data.W - data.a, data.H - data.minB);
        allPossiblePoint.remove(data.allBusyPoints);
        HashSet<Point2D> allFreePoints = allPossiblePoint;

        TreeSet<Point2D> allFreePointsSort = new TreeSet(allFreePoints);

        Iterator<Point2D> pointIterFirst = allFreePointsSort.iterator();

        List<FreeArea> freeAreas = new ArrayList<>();
        FreeArea freeArea = new FreeArea();
        while (pointIterFirst.hasNext()){
            Point2D p1 = pointIterFirst.next();
            pointIterFirst.remove();
            freeArea.points.add(p1);

            
        }
    }
}
