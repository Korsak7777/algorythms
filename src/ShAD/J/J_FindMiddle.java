package ShAD.J;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class J_FindMiddle {

    static class Data {
        int n;
        ArrayList <Double> historicalData;
        int q;
        ArrayList <int[]> querys = new ArrayList();

        String path;

        Data(String path){
            this.path = path;
            readData();
        }

        private void readData() {

            try(BufferedReader br = new BufferedReader(new FileReader(path))) {
                String line = br.readLine();

                int i = 0;
                while (line != null) {
                    if(i == 0){
                        n = Integer.valueOf(line);
                    } else if (i == 1){
                        historicalData = new ArrayList<String>(Arrays.asList(line.split(" ")))
                                        .stream().map(Double::valueOf)
                                        .collect(Collectors.toCollection(ArrayList::new));
                    } else if (i == 2){
                        q = Integer.valueOf(line);
                    } else {
                        String[] queryS = line.split(" ");
                        querys.add(new int[]{Integer.valueOf(queryS[0]), Integer.valueOf(queryS[1])});
                    }
                    line = br.readLine();
                    i++;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public String toString() {
            return "Data{" +
                    "n=" + n +
                    ", historicalData=" + historicalData +
                    ", q=" + q +
                    ", querys=" + querys +
                    '}';
        }
    }

    private static String middle(ArrayList<Double> historicalData, int l, int r){
        double res = 1;
        for (int i = l; i <= r; i++){
            res = res * historicalData.get(i);
        }
        double exponent = 1./(r - l + 1);
        System.out.println(exponent);

        return String.format ("%.6f", Math.pow(res, exponent));
    }

    public static void main(String[] args) throws FileNotFoundException {
        //String path = "C:\\Users\\Dmitriy\\IdeaProjects\\algorythms\\src\\ShAD\\input.txt";
        Data data = new Data("input");

        ArrayList<Double> historicalData = data.historicalData;

        PrintStream out = new PrintStream(new File("output.txt"));

        data.querys.forEach(it -> out.println(middle(historicalData, it[0], it[1])));
    }
}
