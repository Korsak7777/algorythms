package ShAD.K;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class KikLast {
    static class Data {
        int n;
        ArrayList<Integer> data;

        String path;

        Data(String path){
            this.path = path;
            readData();
        }

        private void readData() {
            try(BufferedReader br = new BufferedReader(new FileReader(path))) {
                String line = br.readLine();

                int i = 0;
                while (line != null) {
                    if(i == 0){
                        n = Integer.valueOf(line);
                    } else if (i == 1){
                        data = new ArrayList<String>(Arrays.asList(line.split(" ")))
                                .stream().map(Integer::valueOf)
                                .collect(Collectors.toCollection(ArrayList::new));
                    } 
                    line = br.readLine();
                    i++;
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        @Override
        public String toString() {
            return "Data{" +
                    "n=" + n +
                    ", historicalData=" + data +
                    '}';
        }
    }

    public static String task1(List<Integer> input, String delimiter) {
        Set<Integer> alreadyMet = new HashSet<>(input.size());

        int[] intermidiatValues = new int[input.size()];
        int resultValuesCounter = 0;

        for (int i = input.size() - 1; i >= 0; i--) {
            Integer elem = input.get(i);
            if (alreadyMet.contains(elem))
                intermidiatValues[resultValuesCounter++] = elem;
            else
                alreadyMet.add(elem);
        }
        StringBuilder resultStringBuilder = new StringBuilder(Integer.toString(resultValuesCounter));
        resultStringBuilder.append("\n");
        for(resultValuesCounter--;resultValuesCounter >=0; resultValuesCounter--) {
            if (resultValuesCounter==0){
                resultStringBuilder.append(intermidiatValues[resultValuesCounter]);
            } else {
                resultStringBuilder.append(intermidiatValues[resultValuesCounter]).append(delimiter);
            }
        }

        return resultStringBuilder.toString();
    }

    public static void main(String[] args) throws FileNotFoundException {
        String path = "C:\\Users\\Dmitriy\\IdeaProjects\\algorythms\\src\\ShAD\\K\\input.txt";
        Data data = new Data(path);

        PrintStream out = new PrintStream(new File("output.txt"));

        out.println(task1(data.data, " "));
    }
}
