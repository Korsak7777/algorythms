package helpStructure.scala

//Definition for singly-linked list.
class ListNode(var _x: Int = 0) {
  var next: ListNode = null
  var x: Int = _x

  override def toString() : String = {
    " x = " + x + " (next =" + next + ")"
  }
}