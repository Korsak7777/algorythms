package interview;

import java.util.Arrays;

public class BitwiseAND {
    static void x(){
        int[] m1 = new int[]{-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9};
        int[] m2 = new int[]{-9,-8,-7,-6,-5,-4,-3,-2,-1,0,1,2,3,4,5,6,7,8,9};

        Integer[][] res = new Integer[19][19];

        int i = 0;
        for(int m1El : m1){
            int j = 0;
            for (int m2El : m2){
                res[i][j] = m1El & m2El;
                j++;
            }
            i++;
        }

        print2dArray(res);
    }

    static <K> void print2dArray(K[][] obj){
        for (K[] el : obj){
            System.out.println(Arrays.toString(el));
        }
    }

    public static void main(String[] args) {
        x();

        System.out.println();

        System.out.println(1026 % (16-1));
        System.out.println(1027 % (16-1));
        System.out.println(1028 % (16-1));
        System.out.println(1029 % (16-1));
        System.out.println(1030 % (16-1));
        System.out.println(1031 % (16-1));

        System.out.println();

        System.out.println(1026 & (16-1));
        System.out.println(1027 & (16-1));
        System.out.println(1028 & (16-1));
        System.out.println(1029 & (16-1));
        System.out.println(1030 & (16-1));
        System.out.println(1331 & (16-1));
    }
}
