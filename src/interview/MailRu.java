package interview;

import java.util.*;

public class MailRu {
    public static <K, V> Map<K, V> map() { return new HashMap<>(); }

//
//
//    aaa
//            aac
//->
//    aaa
//            aab
//    aac
//
//
//            zzz
//    aaab
//->
//    zzz
//            aaaa
//    aaab
//
//
//
//            aaz
//    abb
//->
//    aaz
//            aba
//    abb
    
    List<String> x(String s1, String s2){
        ArrayList<String> res = new ArrayList<>();
        res.add(s1);

        int i = 0;
        while(res.get(res.size() - 1) != s2){
            String previous = res.get(i);
            String newElem = getNextAlphabeticString(previous);
            res.add(newElem);

            i++;
        }

        return res;
    }

    private String getNextAlphabeticString(String s){

//        boolean isOnlyZ = false;
//        for(char c : s.toCharArray()){
//            if(c != 'z'){
//                isOnlyZ = true;
//                break;
//            }
//        }
//
//        if(!isOnlyZ)
//            return "";

        String lastChar = s.substring(s.length() - 1);
        String newLastChar;

        String x = "";
        if(lastChar == "z"){
            newLastChar = "a";

            return getNextAlphabeticString(s.substring(0, s.length() - 1)) + newLastChar;
        } else {
            int charValue = lastChar.charAt(0);
            newLastChar = String.valueOf( (char) (charValue + 1));
        }

        return s.substring(0, s.length() - 1) + newLastChar;
    }
}
